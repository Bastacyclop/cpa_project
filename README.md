# CPA project

Computing input points' convex hull with Andrew-Graham,
bounding rectangle with Toussaint,
bounding circle with Ritter.

```sh
# quickly test some input points
cargo run --release --bin test [-- input_path]

# run the entire benchmark
cargo run --release --bin bench
# make the plots
gnuplot < plot_cmds
```

