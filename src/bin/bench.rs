extern crate cpa_project;
extern crate time;

use std::{io, fs, env};
use cpa_project::*;

fn measure<F: FnOnce() -> O, O>(f: F) -> (O, u64) {
    let marker = time::precise_time_ns();
    let o = f();
    (o, time::precise_time_ns() - marker)
}

fn main() {
    use std::io::Write;

    let path = env::args().skip(1).next()
        .unwrap_or_else(|| String::from("bench.data"));
    let file = fs::File::create(path).unwrap();
    let mut writer = io::BufWriter::new(file);

    let mut h_time_acc = 0.;
    let mut h_area_acc = 0.;
    let mut r_time_acc = 0.;
    let mut r_area_acc = 0.;
    let mut c_time_acc = 0.;
    let mut c_area_acc = 0.;
    let mut count = 0;

    for entry in fs::read_dir("samples").unwrap() {
        let path = entry.unwrap().path();
        if path.is_file() {
            let id = path.to_str().unwrap()
                .trim_matches(|c: char| !c.is_numeric())
                .parse::<usize>().unwrap();

            let mut points = input::read_file(&path);
            let (h, h_time) = measure(|| hull(&mut points));
            let h_area = polygon_area(&h);
            let (r, r_time) = measure(|| bounding_rectangle(&h).unwrap());
            let r_area = r.area();
            let (c, c_time) = measure(|| bounding_circle(&points).unwrap());
            let c_area = c.area();

            writeln!(writer, "{} {} {} {} {} {} {}",
                     id,
                     h_time, h_area,
                     r_time, r_area,
                     c_time, c_area).unwrap();

            h_time_acc += h_time as f32;
            h_area_acc += h_area;
            r_time_acc += r_time as f32;
            r_area_acc += r_area;
            c_time_acc += c_time as f32;
            c_area_acc += c_area;
            count += 1;
        }
    }

    println!("benchmarked {} inputs", count);
    println!("average hull time: {} ns", h_time_acc / count as f32);
    println!("average hull area: {}", h_area_acc / count as f32);
    println!("average rectangle time: {} ns", r_time_acc / count as f32);
    println!("average rectangle area: {}", r_area_acc / count as f32);
    println!("average circle time: {} ns", c_time_acc / count as f32);
    println!("average circle area: {}", c_area_acc / count as f32);
}
