extern crate cpa_project;

use std::env;
use cpa_project::*;

fn main() {
    let path = env::args().skip(1).next()
        .unwrap_or_else(|| String::from("samples/test-2.points"));
    let mut points = input::read_file(&path);
    let hull = hull(&mut points);
    output::print_points("hull", &hull);
    println!("with area: {}", polygon_area(&hull));
    let d = diameter(&hull);
    output::print_segment("diameter", d.as_ref().expect("no diameter"));
    let dn = diameter_naive(&hull);
    if d != dn {
        println!("detected diameters difference");
        output::print_segment("diameter_naive", dn.as_ref().expect("no diameter"));
    }
    let r = bounding_rectangle(&hull).expect("no bounding rectangle");
    output::print_rectangle("bounding rectangle", &r);
    println!("with area: {}", r.area());
    let c = bounding_circle(&points).expect("no bounding circle");
    output::print_circle("bounding circle", &c);
    println!("with area: {}", c.area());
}
