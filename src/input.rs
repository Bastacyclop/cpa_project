use std::{io, str, fs};
use std::path::Path;
use nalgebra::Point2;

fn point_from_line(l: &str) -> Point2<i32> {
    let mut split = l.split_whitespace();
    Point2::new(split.next().unwrap().parse().unwrap(),
                split.next().unwrap().parse().unwrap())
}

pub fn read_file<P: AsRef<Path>>(path: P) -> Vec<Point2<i32>> {
    use std::io::BufRead;
    let path = path.as_ref();
    let file = fs::File::open(path).unwrap();
    let mut line = String::new();
    let mut reader = io::BufReader::new(file);

    let points_per_file = 256;
    let mut points = Vec::with_capacity(points_per_file);
    while reader.read_line(&mut line).unwrap() > 0 {
        points.push(point_from_line(&line));
        line.clear();
    }

    println!("read {} points from {:?}", points.len(), path);
    points
}
