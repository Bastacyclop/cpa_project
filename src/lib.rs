extern crate nalgebra;

pub mod input;
pub mod output;

use std::cmp;
use nalgebra::{Scalar, Point2, Vector2, distance_squared};

/// Algorithme d'Andrew-Graham
pub fn hull(points: &mut [Point2<i32>]) -> Vec<Point2<i32>> {
    let points = pixel_filter(points);

    let mut p = points[0];
    let mut hull = vec![p];
    let mut u = Vector2::new(-1, 0);
    for &q in points.iter().chain(points.iter().rev()) {
        if p.y != q.y {
            loop {
                let v = q - p;
                if cross(&vecf32(&u), &vecf32(&v)) <= 0.0 {
                    // moving forward
                    hull.push(q);
                    u = v;
                    p = q;
                    break;
                }

                // moving backward
                hull.pop().unwrap();
                let l = *hull.last().unwrap();
                if hull.len() < 2 {
                    u = Vector2::new(-1, 0);
                } else {
                    u = l - hull[hull.len() - 2];
                }
                p = l;
            }
        }
    }

    hull.pop().unwrap(); // same as the first
    hull
}

fn pixel_filter(points: &mut [Point2<i32>]) -> Vec<Point2<i32>> {
    points.sort_by(|a, b| {
        let ycmp = a.y.cmp(&b.y);
        if ycmp == cmp::Ordering::Equal {
            a.x.cmp(&b.x)
        } else {
            ycmp
        }
    });

    let mut result = Vec::new();
    let mut i = 0;
    while i < points.len() {
        let min = points[i];
        i += 1;
        let mut max = min;
        while i < points.len() {
            let p = points[i];
            if p.y == min.y {
                max = p;
                i += 1;
            } else {
                break;
            }
        }

        result.push(min);
        if max.x != min.x {
            result.push(max);
        }
    }

    result
}

pub fn polygon_area(vertices: &[Point2<i32>]) -> f32 {
    let mut sum = 0.0;
    for (a, b) in vertices.iter().zip(vertices.iter().cycle().skip(1)) {
        sum += cross(&vecf32(&a.coords), &vecf32(&b.coords));
    }
    sum.abs() * 0.5
}

#[derive(Debug, Clone, PartialEq)]
pub struct Circle<N: Scalar> {
    center: Point2<N>,
    radius: N,
}

impl Circle<f32> {
    pub fn area(&self) -> f32 {
        use std::f32::consts::PI;
        PI * self.radius * self.radius
    }
}

/// Algorithme de Ritter
pub fn bounding_circle(points: &[Point2<i32>]) -> Option<Circle<f32>> {
    if points.is_empty() {
        return None;
    }

    let x = &points[0];
    let mut y = x;
    let mut max_dsq = 0.0;
    for p in points{
        let dsq = distance_squared(&pntf32(x), &pntf32(p));
        if dsq > max_dsq {
            y = p;
            max_dsq = dsq;
        }
    }

    let mut z = y;
    let mut max_dsq = 0.0;
    for p in points {
        let dsq = distance_squared(&pntf32(y), &pntf32(p));
        if dsq > max_dsq {
            z = p;
            max_dsq = dsq;
        }
    }

    let mut circle = Circle {
        center: (pntf32(y) + pntf32(z).coords) / 2.0,
        radius: max_dsq.sqrt() / 2.0,
    };

    for p in points {
        let pf = pntf32(p);
        let pc = circle.center - pf;
        let pcnsq = pc.norm_squared();
        if pcnsq > (circle.radius * circle.radius) {
            let pcn = pcnsq.sqrt();
            let r = (pcn + circle.radius) / 2.0;
            circle = Circle {
                center: pf + (pc * (r / pcn)),
                radius: r,
            }
        }
    }

    Some(circle)
}

#[derive(Debug, Clone, PartialEq)]
pub struct Rectangle<N: Scalar> {
    pub a: Point2<N>,
    pub b: Point2<N>,
    pub c: Point2<N>,
    pub d: Point2<N>,
    pub area: f32,
}

impl Rectangle<f32> {
    pub fn area(&self) -> f32 {
        self.area
    }
}

/// Algorithme de Toussaint
pub fn bounding_rectangle(hull: &[Point2<i32>]) -> Option<Rectangle<f32>> {
    if hull.len() < 3 {
        return None;
    }

    let mut ia = 0;
    let mut ib = 1;
    let mut ip = line_antipode(hull, ia, ib, ib);
    let mut iq = ip;
    let mut ir = ia;

    let mut min = (0, 0, 0, 0, 0);
    let mut min_asq = 1. / 0.;
    loop {
        let a = pntf32(&hull[ia]);
        let b = pntf32(&hull[ib]);
        let p = pntf32(&hull[ip]);
        let ab = b - a;
        let abnsq = ab.norm_squared();
        let vz1 = cross(&(p - a), &ab);
        let d1sq = (vz1 * vz1) / abnsq;

        let pab = Vector2::new(-ab.y, ab.x);
        iq = rect_antipode_left(hull, &hull[ip], &pab, iq);
        let q = pntf32(&hull[iq]);
        ir = rect_antipode_right(hull, &hull[ip], &pab, ir);
        let r = pntf32(&hull[ir]);
        let vz2 = cross(&(q - r), &pab);
        let d2sq = (vz2 * vz2) / abnsq;

        let area_sq = d1sq * d2sq;
        if area_sq < min_asq {
            min = (ia, ib, ip, iq, ir);
            min_asq = area_sq;
        }

        if ib == 0 { break; }
        ia = ib;
        ib = (ib + 1) % hull.len();
        ip = line_antipode(hull, ia, ib, ip);
    }

    let a = pntf32(&hull[min.0]);
    let b = pntf32(&hull[min.1]);
    let p = pntf32(&hull[min.2]);
    let q = pntf32(&hull[min.3]);
    let r = pntf32(&hull[min.4]);
    let ab = b - a;
    let pab = Vector2::new(-ab.y, ab.x);
    Some(Rectangle {
        a: intersection(&q, &pab, &a, &ab),
        b: intersection(&q, &pab, &p, &ab),
        c: intersection(&r, &pab, &p, &ab),
        d: intersection(&r, &pab, &a, &ab),
        area: min_asq.sqrt(),
    })
}

fn rect_antipode<F>(hull: &[Point2<i32>], vz_check: F,
                    s: &Point2<i32>, d: &Vector2<f32>, from: usize) -> usize
    where F: Fn(f32) -> bool
{
    let dnsq = d.norm_squared();
    let mut ip = from;
    let mut max_dsq = 0.0;
    let mut checked = false;
    loop {
        let p = &hull[ip];
        let sp = vecf32(&(p - s));
        let vz = cross(&sp, &d);
        if vz_check(vz) {
            let dsq = (vz * vz) / dnsq;
            if dsq < max_dsq {
                return (ip + hull.len() - 1) % hull.len();
            }

            max_dsq = dsq;
            checked = true;
        } else if checked {
            return (ip + hull.len() - 1) % hull.len();
        }

        ip = (ip + 1) % hull.len();
    }
}

fn rect_antipode_left(hull: &[Point2<i32>],
                      s: &Point2<i32>, d: &Vector2<f32>, from: usize) -> usize {
    rect_antipode(hull, |vz| vz < 0.0, s, d, from)
}

fn rect_antipode_right(hull: &[Point2<i32>],
                       s: &Point2<i32>, d: &Vector2<f32>, from: usize) -> usize {
    rect_antipode(hull, |vz| vz > 0.0, s, d, from)
}

fn line_antipode(hull: &[Point2<i32>],
                 is: usize, it: usize, from: usize) -> usize {
    let s = &hull[is];
    let t = &hull[it];
    let d = vecf32(&(t - s));
    let dnsq = d.norm_squared();

    let mut ip = from;
    let mut max_dsq = 0.0;
    loop {
        let p = &hull[ip];
        let sp = vecf32(&(p - s));
        let vz = cross(&sp, &d);
        let dsq = (vz * vz) / dnsq;
        if dsq < max_dsq {
            return (ip + hull.len() - 1) % hull.len();
        }

        max_dsq = dsq;
        ip = (ip + 1) % hull.len();
    }
}

fn cross(a: &Vector2<f32>, b: &Vector2<f32>) -> f32 {
    a.x * b.y - a.y * b.x
}

fn pntf32(p: &Point2<i32>) -> Point2<f32> {
    Point2::new(p.x as f32, p.y as f32)
}

fn vecf32(v: &Vector2<i32>) -> Vector2<f32> {
    Vector2::new(v.x as f32, v.y as f32)
}

/// Hypothesis: not colinear
fn intersection(sa: &Point2<f32>, da: &Vector2<f32>,
                sb: &Point2<f32>, db: &Vector2<f32>) -> Point2<f32> {
    let num = da.y * (sb.x - sa.x) - da.x * (sb.y - sa.y);
    let den = da.x * db.y - da.y * db.x;
    let v = num / den;
    sb + v * db
}

#[derive(Debug, Clone, PartialEq)]
pub struct Segment<N: Scalar> {
    pub beg: Point2<N>,
    pub end: Point2<N>,
}

pub fn diameter_naive(hull: &[Point2<i32>]) -> Option<Segment<i32>> {
    let mut max = None;
    let mut max_dsq = 0.0;
    for (i, b) in hull.iter().enumerate() {
        for r in &hull[(i + 1)..] {
            let dsq = distance_squared(&pntf32(b), &pntf32(r));
            if dsq > max_dsq {
                max = Some(Segment {
                    beg: *b,
                    end: *r,
                });
                max_dsq = dsq;
            }
        }
    }

    max
}

pub fn diameter(hull: &[Point2<i32>]) -> Option<Segment<i32>> {
    if hull.len() < 3 {
        return None;
    }

    let ia = hull.len() - 1;
    let mut ib = 0;
    let mut ic = 1;
    let mut ip = line_antipode(hull, ib, ia, ib);
    let mut iq = line_antipode(hull, ic, ib, ip);

    let mut max = None;
    let mut max_dsq = 0.0;
    loop {
        let b = hull[ib];
        let mut ir = ip;
        loop {
            let r = hull[ir];
            let dsq = distance_squared(&pntf32(&b), &pntf32(&r));
            if dsq > max_dsq {
                max = Some(Segment {
                    beg: b,
                    end: r,
                });
                max_dsq = dsq;
            }

            if ir == iq { break; }
            ir = (ir + 1) % hull.len();
        }

        ib = ic;
        ic = (ic + 1) % hull.len();
        ip = iq;
        let next_iq = line_antipode(hull, ic, ib, ip);
        if next_iq < iq { break; }
        iq = next_iq;
    }

    max
}
