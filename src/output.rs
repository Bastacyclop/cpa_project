use std::fmt;
use nalgebra::{Scalar, Point2};
use {Segment, Rectangle, Circle};

pub fn print_points<N: Scalar + fmt::Display>(name: &str, ps: &[Point2<N>]) {
    print!("{}: [", name);
    let mut i = ps.iter();
    if let Some(p) = i.next() {
        print!("{}", p);
        for p in i {
            print!(", {}", p);
        }
    }
    println!("]");
}

pub fn print_segment<N: Scalar + fmt::Display>(name: &str, s: &Segment<N>) {
    println!("{}: Segment {{ {}, {} }}", name, s.beg, s.end);
}

pub fn print_rectangle<N: Scalar + fmt::Display>(name: &str, r: &Rectangle<N>) {
    println!("{}: Rectangle {{ {}, {}, {}, {} }}", name, r.a, r.b, r.c, r.d);
}

pub fn print_circle<N: Scalar + fmt::Display>(name: &str, c: &Circle<N>) {
    println!("{}: Circle {{ center: {}, radius: {} }}", name, c.center, c.radius);
}
